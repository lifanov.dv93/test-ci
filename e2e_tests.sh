#!/bin/sh

#git checkout ${E2E_TESTS_BRANCH}
npm ci --cache .npm --prefer-offline
PLAYWRIGHT_BROWSERS_PATH=/ms-playwright npx playwright install chromium
mkdir -p $(dirname "$ARTIFACT_FILEPATH")
npm run test:custom-runner

# Join all results in one file and change path for screenshots
status=$?
#npm run merge $ARTIFACT_FILEPATH
npx junit-merge -o $ARTIFACT_FILEPATH/junit-result/result.xml -d $ARTIFACT_FILEPATH/junit-result/
sed -i 's#ATTACHMENT|/builds/qa/codeceptJS#ATTACHMENT|#g' $ARTIFACT_FILEPATH/junit-result/result.xml
exit $status
