const { actor } = require('codeceptjs');

module.exports = () => actor({

  /**
   * Check toggle position
   *
   * @param {string} nameOrId - specify name of toggle if element input in DOM has name
   * or field id with toggle if not
   * @param {boolean|string} value - specify false if the toggle position is off or true if it is on
   * @param {boolean} [disabled=false] - specify true if the toggle disabled
   */
  checkTogglePosition(nameOrId, value, disabled = false) {
    this.seeElementInDOM(`//input[@name="${nameOrId}"][@value="${value}"]${disabled ? '[@disabled]' : ''} | //*[@id="${nameOrId}"]//input[@value="${value}"]${disabled ? '[@disabled]' : ''}`);
  },

  /**
   * Wait for visible and then click
   *
   * @param {string} locator
   */
  clickAfterVisible(locator) {
    this.waitForVisible(locator);
    this.click(locator);
  },

  /**
   * Click on link and switch to the next tab
   *
   * @param {string} locator
   * @param {object} configs - configs
   * @param {Function | null} [configs.loaderFunction=null] - loader after click
   * @param {string | null} [configs.seeElement=null] - locator in loader
   * @param {boolean} [configs.shouldWaitForVisible=true] - wait for visible before click
   */
  // TODO ACCEPTANCE-13321
  async clickAndSwitchToNextTab(locator, {
    loaderFunction = null,
    seeElement = null,
    shouldWaitForVisible = true,
  } = {}) {
    if (shouldWaitForVisible) {
      this.waitForVisible(locator);
    }
    const tabsBeforeClick = await this.grabNumberOfOpenTabs();
    const attemptsLimit = 2;
    for (let attempt = 1; attempt < attemptsLimit; attempt++) {
      this.say(`Attempt ${attempt}`);
      this.clickAfterVisible(locator);
      if (loaderFunction && seeElement) {
        loaderFunction(seeElement);
      } else if (loaderFunction) {
        loaderFunction();
      }
      let tabsAfterClick;
      const maxIterations = 50;
      const timeout = 100;
      for (let i = 0; i < maxIterations; i++) {
        tabsAfterClick = await this.grabNumberOfOpenTabs();
        await new Promise((resolve) => {
          setTimeout(resolve, timeout);
        });
        if (tabsAfterClick > tabsBeforeClick) {
          this.switchToNextTab();
          this.say(`A new tab was opened for about ${(i + 1) * timeout}ms`);
          break;
        }
        if (i + 1 === maxIterations) {
          const logs = await this.grabBrowserLogs();
          const errors = logs.map((item) => ({ text: item.text(), type: item.type() })).filter((item) => item.type === 'error');
          this.say(`Attempt ${attempt} has failed! Console errors: ${JSON.stringify(errors)}`);
        }
        if (attempt === attemptsLimit) {
          throw new Error(`A new tab was not opened for ${attemptsLimit * maxIterations * timeout}ms!`);
        }
      }
    }
  },

  /**
   * Click on all elements matching by locator,
   * function can be used when elements locator has changing after click
   *
   * @param {string} locatorBeforeClick - elements locator before click
   * @param {string} locatorAfterClick - elements locator after click
   */
  async clickOnAllElements(locatorBeforeClick, locatorAfterClick) {
    const amount = await this.grabNumberOfVisibleElements(locatorBeforeClick);
    for (let i = 0; i < amount; i++) {
      this.clickAfterVisible(locatorBeforeClick);
      this.waitNumberOfVisibleElements(locatorAfterClick, i + 1);
    }
  },

  /**
   * Fill field and wait value in field
   *
   * @param {string} field
   * @param {string|number|secret} value
   * @param {object} [optional]
   * @param {boolean} [optional.isBlur=false]
   */
  customFillField(field, value, { isBlur = false } = {}) {
    if (value !== undefined) {
      this.fillField(field, value);
      if (isBlur) {
        this.blur(field);
      }
    }
  },

  /**
   * Click download file and wait loader
   *
   * @param {string} locator - locator button
   * @param {string} file - file name
   * @param {object} configs - configs
   * @param {Function | null} [configs.loaderFunction=null] - loader after click
   * @param {string | null} [configs.seeElement=null] - locator in loader
   */
  async downloadFile(locator, file, { loaderFunction = null, seeElement = null } = {}) {
    if (file.endsWith('.pdf')) {
      await this.setDownloadAttribute(locator);
    }
    await this.handleDownloads(`downloads/${file}`);
    this.clickAfterVisible(locator);
    if (loaderFunction && seeElement) {
      loaderFunction(seeElement);
    } else if (loaderFunction) {
      loaderFunction();
    }
  },

  /**
   * Expand element by click on button Show more or Expand
   *
   * @param {string} [parentElement='']
   * @param {object} flag
   * @param {boolean} [flag.expandAll=false]
   */
  async expandElement(parentElement = '', { expandAll = false } = {}) {
    const showMore = `${parentElement}${buttons.showMore}`;
    const showLess = `${parentElement}//..${buttons.showLess}`;
    if (expandAll === false) {
      this.clickAfterVisible(showMore);
      this.waitForElement(showLess);
    } else {
      await this.clickOnAllElements(showMore, showLess);
    }
  },

  /**
   * Clear field and then fill value (Vue.js)
   * TODO delete this method after move to react Resell offer and Affiliate request
   *
   * @param {string} field
   * @param {string} value
   */
  async fillFieldVue(field, value) {
    const closeIcon = await this.grabAttributeFrom(`${field}/following-sibling::span`, 'class');
    if (!closeIcon.includes('iconHidden')) {
      // eslint-disable-next-line internal/contains-in-xpath
      this.waitForElement(`${field}/following-sibling::*[contains(@class, 'mdi-close')]`);
      // eslint-disable-next-line internal/contains-in-xpath
      this.click(`${field}/following-sibling::*[contains(@class, 'mdi-close')]`);
      // eslint-disable-next-line internal/contains-in-xpath
      this.waitForElement(`${field}/following-sibling::*[contains(@class, 'mdi-close') and contains(@class, 'Autocomplete_iconHidden')]`);
    }
    this.customFillField(field, value);
  },

  /**
   * Fill field using data-testid
   *
   * @param {string} dataTestId
   * @param {string} value
   * @param {object} optional
   * @param {string}[optional.element='input']
   */
  fillFieldWithDataTestId(dataTestId, value, { element = 'input' } = {}) {
    this.customFillField(`//*[@data-testid="${dataTestId}"]//${element}`, value);
  },

  /**
   * Grab formatted text without empty lines
   *
   * @param {string} locator - xPath or css locator
   * @returns {Promise<string>} text in locator
   */
  async grabFormattedText(locator) {
    const actualText = await this.grabTextFromAll(locator);
    return Promise.resolve(actualText.map((item) => item.replace(/^\s*|null$/gm, '')
      .replace(/[\u00a0\u2000-\u200a]+|\t+/g, ' ')
      .trim())
      .join('\n'));
  },

  /**
   * Is checkbox checked? Use only with locator having 'checked' class
   *
   * @param {string} locator - xPath or css locator
   * @returns {Promise<boolean>} - true or false
   */
  async isCheckboxChecked(locator) {
    const classes = await this.grabAttributeFrom(locator, 'class');
    const result = classes.includes('checked');
    await this.say(`Is checkbox checked? ${result}`);
    return Promise.resolve(result);
  },

  /**
   * Save changes by click button Save change or button Save
   *
   * @param {string} [button = buttons.saveChanges]
   * @param {string} [successfullySavedText='']
   */
  saveChanges(button = buttons.saveChanges, successfullySavedText = '') {
    this.click(button);
    loader.loaderAdmin(successfullySavedText !== '' ? `//p[.="${successfullySavedText}"]` : '.ready');
  },

  /**
   * Set throttling
   *
   * @param {number} [downloadSpeed=1] - speed in Mb/s
   * @param {number} [uploadSpeed=1] - speed in Mb/s
   * @param {number} [latency=10] - latency in ms
   */
  async setSpeedConnection(downloadSpeed = 1, uploadSpeed = 1, latency = 10) {
    const pw = await this.usePlaywrightTo('pw', async (Playwright) => Playwright);
    const { page } = pw;
    const client = await page.context().newCDPSession(page);
    await client.send('Network.enable');
    await client.send('Network.emulateNetworkConditions', {
      downloadThroughput: (downloadSpeed * 1024 * 1024) / 8,
      latency,
      offline: false,
      uploadThroughput: (uploadSpeed * 1024 * 1024) / 8,
    });
  },

  /**
   * Switch toggle to 'active' or 'passive' position
   *
   * @param {string} nameOrId - specify name of toggle if element input in DOM has name
   * or field id with toggle if not
   * @param {boolean|string} isActive - specify expected toggle position
   */
  async switchToggle(nameOrId, isActive) {
    const toggleElement = `//input[@name="${nameOrId}"] | //*[@id="${nameOrId}"]//input`;
    const currentPosition = await this.grabValueFrom(toggleElement);
    if (currentPosition !== `${isActive}` && isActive !== undefined) {
      this.waitForElement(`//input[@name="${nameOrId}"]/following-sibling::* | //*[@id="${nameOrId}"]//input/following-sibling::*`);
      this.click(`//input[@name="${nameOrId}"]/following-sibling::* | //*[@id="${nameOrId}"]//input/following-sibling::*`);
      this.waitForElement(`//input[@name="${nameOrId}"][@value="${isActive}"] | //*[@id="${nameOrId}"]//input[@value="${isActive}"]`);
    }
  },
});
