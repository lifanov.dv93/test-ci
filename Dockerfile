FROM codeceptjs/codeceptjs:3.5.12

# Установка curl и jq и чистка кеша после установки
RUN apt update --fix-missing && apt install -y curl jq && rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY . /app
RUN npm install
