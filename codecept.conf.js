const URI_PREFIX = process.env.URI_PREFIX || 'local';
const DOMAIN = process.env.DOMAIN || 'developzilla.com';
const MOCKSERVER_HOST = process.env.MOCKSERVER_HOST || 'localhost';
const GREP = process.env.GREP || '.*';
const TRACE = process.env.TRACE || 'false';
const TESTS_PATH = process.env.TESTS_PATH || './tests/**/*_test.js';

// const { container } = require('codeceptjs');
// const { inspect } = require('util');
const include = require('./include.config');
// const endpoint = require('./misc/urls');
// const bootstrap = require('./bootstrap');

exports.config = {
  // bootstrap: async () => {
  //   await bootstrap.bootstrap();
  // },
  grep: GREP,
  helpers: {
    // $ConsoleHelper: {
    //   enabled: true,
    //   require: './helpers/console_helper.js',
    //   searchClientErrors: true,
    // },
    // AllureVideoHelper: {
    //   require: './helpers/allure_video_helper.js',
    // },
    // FileHelper: {
    //   expectedPath: `${process.cwd()}/resources/files`,
    //   path: './output/downloads',
    //   require: './helpers/file_helper.js',
    //   wait: 30,
    // },
    // FileSystem: {},
    // MailhogHelper: {
    //   baseURL: `http://mailhog.${URI_PREFIX}.${DOMAIN}/api`,
    //   pollingInterval: 10000,
    //   require: './helpers/mailhog_helper.js',
    //   waitForTimeout: 150000,
    // },
    // MockServerHelper: {
    //   host: MOCKSERVER_HOST,
    //   port: 1080,
    //   require: './helpers/mock_server_helper.js',
    //   waitForTimeout: 20000,
    // },
    Playwright: {
      getPageTimeout: 30000,
      keepVideoForPassedTests: false,
      recordVideo: {
        size: { height: 576, width: 1024 },
      },
      restart: false,
      show: true,
      testDir: './tests',
      timeout: 20000,
      trace: TRACE,
      url: 'http://localhost',
      waitForAction: 100,
      waitForNavigation: 'load',
      waitForTimeout: 10000,
      windowSize: '1280x720',
    },
    // REST: {
    //   endpoint,
    //   onResponse: async (resp) => {
    //     const { allure } = container.plugins();
    //     await allure.createAttachment('Response', `{ status: ${resp.status},\nstatusText: ${resp.statusText},\nheaders: ${inspect(resp.headers, true, 10)},\ndata: ${inspect(resp.data, true, 10)} }`, 'text/plain');
    //   },
    //   prettyPrintJson: true,
    // },
  },
  include,
  mocha: {
    reporter: 'mocha-multi',
    reporterOptions: {
      'codeceptjs-cli-reporter': {
        options: {
          steps: true,
        },
        stdout: '-',
      },
      'mocha-junit-reporter': {
        options: {
          attachments: true,
          mochaFile: './junit-result/result[hash].xml',
        },
        stdout: '-',
      },
    },
  },
  name: 'simple',
  output: './output',
  plugins: {
    allure: {
      enabled: true,
      outputDir: './output/allure-results',
      require: '@codeceptjs/allure-legacy',
    },
    // retryFailedStep: {
    //   enabled: true,
    //   retries: 2,
    // },
    screenshotOnFail: {
      enabled: true,
      fullPageScreenshots: true,
      uniqueScreenshotNames: true,
    },
  },
  tests: TESTS_PATH,
};
