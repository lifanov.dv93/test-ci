const { execSync, spawn } = require('child_process')
const { output } = require('codeceptjs');
const { inspect } = require('util');
const { getTests, getFiles, distributeTests } = require('./functions');
const { config } = require('./codecept.conf.ci');

const THREADS = process.env.THREADS || '1';
const THREAD_INDEX = process.env.THREAD_INDEX || '1';

// const testRoot = './tests';
const testRoot = config.helpers.Playwright.testDir;

// process.addListener("message", execSync);
// execSync('npx codeceptjs dry-run');
// process.listeners('message')

const files = getFiles(testRoot);
const tests = getTests(files);
output.print(inspect(tests));
const streamTests = distributeTests(THREADS, THREAD_INDEX, tests);
output.print(inspect(streamTests));

// output.print(streamTests.map((item) => item.filename).toString().replace(/'/g, '"'));

const testsFiles = JSON.stringify(streamTests.map((item) => item.filename.replace('./', '')));
output.print(testsFiles)

// config.tests = streamTests;
// config.helpers.Playwright.testDir = streamTests;

// output.print(inspect(config.tests))
// output.print(inspect(config.helpers.Playwright.testDir))

// достаём из конфига количество потоков и индекс потока
// определяем количество тестов в каждом файле проекта
// делим количество тестов на количество потоков
// для каждого потока надо запустить свои тесты:
// - как запустить Х? тестов для второй группы. Кажется, что без грепа не обойтись. Можно попробовать создавать массив файлов, внутри которых будет нужное количество тестов
// - (если нет, то) можно ли запускать АТ с грепом 300 тестов? Есть ли лимит по длине?

try {
    // const res = execSync(`npx codeceptjs run-workers 3 --config codecept.conf.ci.js --reporter mocha-multi -- --debug`);
    const res = execSync(`npx codeceptjs run-workers 2 --config codecept.conf.ci.js --override '{"tests": ${testsFiles}}' --reporter mocha-multi`);
    output.print(res.toString());
}
catch (err){
    output.error("output", err)
    output.error("sdterr",err.stderr.toString())
}


// const res = spawn('npx codeceptjs run-workers 2', ['--config', 'codecept.conf.ci.js', '--override', `{"tests": [${testsFiles}]}`, '--reporter', 'mocha-multi', '--debug']);
//
// res.stdout.on('data', (data) => {
//     console.log(`stdout: ${data}`);
// });
//
// res.stderr.on('data', (data) => {
//     console.error(`stderr: ${data}`);
// });
//
// res.on('close', (code) => {
//     console.log(`child process exited with code ${code}`);
// });
