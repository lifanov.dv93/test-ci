const { output, container } = require('codeceptjs');
const fs = require('fs')

// Recursive function to get files
function getFiles(dir, files = []) {
    // Get an array of all files and directories in the passed directory using fs.readdirSync
    const fileList = fs.readdirSync(dir)
    // Create the full path of the file/directory by concatenating the passed directory and file/directory name
    for (const file of fileList) {
        const name = `${dir}/${file}`
        // Check if the current file/directory is a directory using fs.statSync
        if (fs.statSync(name).isDirectory()) {
            // If it is a directory, recursively call the getFiles function with the directory path and the files array
            getFiles(name, files)
        } else {
            // If it is a file, push the full path to the files array
            // files.push(name.slice(name.lastIndexOf('/')))
            files.push(name);
        }
    }
    return files
}

function getTests(files) {
    const tests = files.map((file) => {
        const result = {
            filename: file,
            amount: fs.readFileSync(file).toString().match(/Scenario.+ \{/g).length,
        }
        return result;
    });
    return tests;
}

/**
 *
 * @param streamsAmount
 * @param streamIndex
 * @param tests
 * @returns {object[]} threadsTests
 */
function distributeTests(streamsAmount, streamIndex, tests) {
    const sortedTests = tests.sort((test1, test2) => test2.amount - test1.amount);
    // const totalTests = sortedTests.reduce((total, test) => {
    //     total += test.amount;
    //     return total;
    // }, 0);
    // const streamTestsAmount = totalTests / Number(streamsAmount);
    const threadsTests = distribute(sortedTests, Array(Number(streamsAmount)).fill('').map(() => []))
    return threadsTests[Number(streamIndex) - 1];
}

function distribute(sortedTests, threads) {
    // Перебираем каждое значение во входном массиве
    for (const test of sortedTests) {
        // Находим подмассив с наименьшим количеством тестов
        let minSum = Infinity;
        let minIndex = -1;
        for (let i = 0; i < threads.length; i++) {
            const subarraySum = threads[i].reduce((acc, curr) => acc + curr.amount, 0);
            if (subarraySum < minSum) {
                minSum = subarraySum;
                minIndex = i;
            }
        }

        // Добавляем значение в подмассив с наименьшим количеством тестов
        threads[minIndex].push(test);
    }
    return threads;
}

module.exports = {
    getTests,
    getFiles,
    distributeTests,
}
